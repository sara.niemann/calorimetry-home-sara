from functions import m_json
from functions import m_pck

path = "/home/pi/calorimetry-home-sara/datasheets/setup_newton.json"
dictionary = m_json.get_metadata_from_setup(path)
m_json.add_temperature_sensor_serials('/home/pi/calorimetry-home-sara/datasheets', dictionary)

print(dictionary)
messdata = m_pck.get_meas_data_calorimetry(dictionary)
m_pck.logging_calorimetry(messdata, dictionary, "/home/pi/calorimetry-home-sara/data/newton", "/home/pi/calorimetry-home-sara/datasheets")

